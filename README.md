# JavaScript Classes

## Tasks

#### Rectangle
Return the rectagle object with width and height parameters and getArea() method.
For example:
```js
var r = new Rectangle(10,20);
console.log(r.width);       // => 10
console.log(r.height);      // => 20
console.log(r.getArea());   // => 200
```
Write your code in `src/index.js` within an appropriate function `Rectangle`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### JSON representation
Return the JSON representation of specified object.
For example:
```js
[1,2,3]   =>  '[1,2,3]'
{ width: 10, height : 20 } => '{"height":10,"width":20}'
```
Write your code in `src/index.js` within an appropriate function `getJSON`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### From JSON
Return the object of specified type from JSON representation.
For example:
```js
var r = fromJSON(Rectangle.prototype, '{"width":10, "height":20}');
```
Write your code in `src/index.js` within an appropriate function `fromJSON`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-classes
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-classes/  
4. Go to folder `javascript-classes`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-classes)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
